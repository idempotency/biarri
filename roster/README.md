This is Ryan Pollard's technical exercise for Biarri.

## Running the App

```make serve-json```

Serves the JSON required by the app.

```make serve```

Builds and serves the web app.

## Completed Requirements

- The app should use React as a base framework.
- The app should display the whole week of rostering information.
- The roster must be represented in both a tabular format, and also a visualisation of some form.
- Dates are formatted to the provided UTC offset.

## Incomplete Requirements

- The ability to edit the start/end times of a shift.