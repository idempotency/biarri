import React from 'react';
import styled from 'react-emotion';
import { Table } from './components/Table';
import api from './api/api';
import moment from 'moment-timezone';
import { groupBy, mapValues, map } from 'lodash';
import gear from './images/gear.gif';
import { Rosters } from './components/Rosters'

const Container = styled("div")`
  height: 100vh;
  display: grid;
  justify-items: center;
  grid-row-gap: 3rem;
  font-family: 'Open Sans', sans-serif;
  font-weight: 300;
`;

const Title = styled("div")`
  font-size: 3rem;
  margin-top: 2rem;
`;

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {shifts: []};
  }

  getData = async() => (
    Promise.all([
      await api.get('/config'),
      await api.get('/employees'),
      await api.get('/roles'),
      await api.get('/shifts'),
    ]).then(([config, employees, roles, shifts]) => {
      const realTime = (time) => moment(time).tz(config.data.timezone);
      this.setState({
        shifts: shifts.data.map(s => (
          {
            id: s.id,
            employee: employees.data.find(e => e.id === s.employee_id),
            role: roles.data.find(r => r.id === s.role_id),
            time: { 
              start: realTime(s.start_time).format('HHmm'),
              end: realTime(s.end_time).format('HHmm'), 
            },
            break: s.break_duration / 60,
            day: {
              start: realTime(s.start_time).format('ddd'),
              end: realTime(s.end_time).format('ddd'),
            },
            week: realTime(s.start_time).format('w'),
          }
        ))
      });
    })
  )

  componentDidMount() {
    this.getData()
      .catch((err) => console.log(err));
  }

  render() {
    return (
      <Container>
        <Title>Rostering WebApp</Title>
        {
          this.state.shifts.length
          ? <>
              <Rosters data=
                {map(mapValues(groupBy(this.state.shifts, 'week'), week => groupBy(week, 'day.start')), (days, week) => (
                  {
                    days: days,
                    week: week,
                  }
                ))}
              />
              <Table
                key="table"
                data={this.state.shifts}
              />
            </>
          : <img src={gear} alt="loading"/>
        }
      </Container>
    )
  }
}

export default App;
