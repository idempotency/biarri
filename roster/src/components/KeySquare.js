import React from 'react';
import styled from 'react-emotion';

const Square = styled("div")`
  width: 1rem;
  height: 1rem;
  display: inline-block;
  margin-right: 0.2rem;
  background-color: ${ props => 
    props.position === "Checkouts"
      ? "#f5ffa2"
      : props.position === "Supervisor"
        ? "#f38f60"
        : props.position === "Stacker"
          ? "#bf8ef1"
          : "white"
  };
`;

const Text = styled("span")`

`;

const UKeySquare = ({ position, className }) => (
  <div className={className}>
    <Square position={position}/>
    <Text>{position}</Text>
  </div>
)

export const KeySquare = styled(UKeySquare)`
  margin-left: 1rem;
`;