import React from 'react';
import styled from 'react-emotion';
import { map } from 'lodash';
import Color from 'color';
import { KeySquare } from './KeySquare';

const Column = styled("div")`

`;

const Day = styled("div")`
  font-size: 1.5rem;
  text-align: center;
  margin-bottom: 0.5rem;
`;

const Shift = styled("div")`
  padding: 0.5rem;
  margin: 0.3rem 0;
  background-color: ${props => Color(props.color).lighten(0.15).string()};
  min-height: 5rem;
  text-align: center;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  border-radius: 5px;
  user-select: none;
  cursor: pointer;
  &:hover {
    background-color: ${props => props.color};
    box-shadow: 2px 2px 5px #ccc;
  }
  &:active {
    transform: translate(0px, 2px);
    box-shadow: none;
  }
`;

const Name = styled("div")`
  font-weight: 400;
`;

const Time = styled("div")`
  font-size: 0.8rem;
`;

const Key = styled("div")`
  grid-column-start: span 7;
  display: flex;
  padding-top: 2rem;
`;

const URoster = ({ className, week, days, label }) => (
  <div className={className} label={label}>
    {map(days, (shifts, day) => (
      <Column key={day}>
        <Day>{day}</Day>
        {map(shifts, (s) => (
          <Shift key={s.id} color={s.role.background_colour}>
            <Name>{`${s.employee.first_name} ${s.employee.last_name}`}</Name>
            <Time>{`${s.time.start} - ${s.time.end}`}</Time>
          </Shift>
        ))}
      </Column>
    ))}
    <Key>
      <KeySquare position="Checkouts"/>
      <KeySquare position="Supervisor"/>
      <KeySquare position="Stacker"/>
    </Key>
  </div>
);

export const Roster = styled(URoster)`
  display: grid;
  grid-column-gap: 2rem;
  grid-template-columns: repeat(7, 1fr);
  justify-items: center;
  padding: 2rem;
`;