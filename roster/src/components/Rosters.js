import React, { Component } from 'react';
import { Roster } from './Roster';

import { Tab } from './Tab';
import styled from 'react-emotion';

const Tabs = styled("div")`
  display: flex;
  justify-content: space-around;
`;

class URosters extends Component {
  constructor(props) {
    super(props);

    this.state = {
      activeTab: this.props.data[0].week
    };
  }

  onClickTab = (tab) => {
    this.setState({ activeTab: tab });
  }

  render() {
    const {
      onClickTab,
      props: {
        className,
        data,
      },
      state: {
        activeTab,
      }
    } = this;

    return (
      <div className={className}>
        <Tabs>
          {data.map((d) => (
            <Tab
              activeTab={activeTab}
              key={d.week}
              text={d.week}
              onClick={onClickTab}
            />
          ))}
        </Tabs>
        <div>
          {data.map((d) => !(d.week !== activeTab)
            && <Roster key={(d.week)} days={d.days}></Roster>
          )}
        </div>
      </div>
    );
  }
}

export const Rosters = styled(URosters)`
  box-shadow: 2px 2px 10px #ccc;
  border-radius: 10px 10px 0 0 ;
`;