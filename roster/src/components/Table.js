import React from 'react';
import styled from 'react-emotion';

const UTable = ({ className, data }) => (
  <table className={className}>
    <thead>
      <tr>
        <th>Week</th>
        <th>Start of Shift</th>
        <th>End of Shift</th>
        <th>Employee Name</th>
        <th>Role</th>
        <th>Allocated Break</th>
      </tr>
    </thead>
    <tbody>
      {data.map(x => (
        <tr key={x.id}>
          <td>{x.week}</td>
          <td>{`${x.day.start} ${x.time.start}`}</td>
          <td>{`${x.day.end} ${x.time.end}`}</td>
          <td>{`${x.employee.first_name} ${x.employee.last_name}`}</td>
          <td>{x.role.name}</td>
          <td>{`${x.break} mins`}</td>
        </tr>
      ))}
    </tbody>
  </table>
);

export const Table = styled(UTable)`
  box-shadow: 2px 2px 10px #ccc;
  text-align: right;
  border-collapse: collapse;
  th, td {
    padding: 0.2rem 1rem;
    border-bottom: 1px solid #ccc;
  }
`;