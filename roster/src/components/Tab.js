import React from 'react';
import styled from 'react-emotion';

const UTab = ({ text, className, onClick}) => (
  <div
    className={className}
    onClick={() => onClick(text)}
  >
    {`Week ${text}`}
  </div>
)

export const Tab = styled(UTab)`
  font-weight: ${props => props.activeTab === props.text && "bold"};
  background-color: ${props => !(props.activeTab === props.text) && "#ddd"};
  box-shadow: ${props => !(props.activeTab === props.text) && "inset 0px -10px 10px -10px #aaa"};
  cursor: pointer;
  text-align: center;
  width: 100%;
  padding: 1rem;
  font-size: 1.5rem;
  &:hover {
    background-color: ${props => !(props.activeTab === props.text) && "#eee"};
  }
`;